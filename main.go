package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"regexp"
	"sort"
	"strings"
	"time"

	"github.com/microsoft/azure-devops-go-api/azuredevops"
	"github.com/microsoft/azure-devops-go-api/azuredevops/core"
	"github.com/microsoft/azure-devops-go-api/azuredevops/git"
)

// todo: add skip feature
// todo: code review, perhaps delete map usage
// todo: lookup why DEVPD-18590 is duplicated, maybe also allow multiple-ticket-titles

var userName *string
var organizationURLSlug *string
var personalAccessToken *string
var projectName *string
var repositoryName *string
var ticketPrefix *string
var prAmountToQuery *int // Use less than 100

func main() {
	setupAndCheckFlags()

	// Create a connection to your organization
	connection := azuredevops.NewPatConnection("https://dev.azure.com/"+*organizationURLSlug, *personalAccessToken)

	ctx := context.Background()

	// Create a client to interact with the Core area
	coreClient, err := core.NewClient(ctx, connection)
	if err != nil {
		log.Fatal(err)
	}

	projects, err := coreClient.GetProjects(ctx, core.GetProjectsArgs{})
	if err != nil {
		log.Fatal(err)
	}

	if *projectName == "" {
		projectNames := mapProjectSliceToStringSlice(projects.Value, func(project core.TeamProjectReference) string {
			return *project.Name
		})
		log.Fatalln("You have not selected a projectName yet, these are the available ones:\n", projectNames)
	}

	projectID, err := findProjectID(projects.Value, *projectName)
	if err != nil {
		log.Fatal(err)
	}

	coreClient.GetProject(ctx, core.GetProjectArgs{ProjectId: &projectID})
	if err != nil {
		log.Fatal(err)
	}

	gitClient, err := git.NewClient(ctx, connection)
	if err != nil {
		log.Fatal(err)
	}

	repositoriesResponse, err := gitClient.GetRepositories(ctx, git.GetRepositoriesArgs{Project: &projectID})
	if err != nil {
		log.Fatal(err)
	}

	if *repositoryName == "" {
		repositoryNames := mapRepositorySliceToStringSlice(*repositoriesResponse, func(repository git.GitRepository) string {
			return *repository.Name
		})
		log.Fatalln("You have not selected a repositoryName yet, these are the available ones:\n", repositoryNames)
	}

	repository, err := findRepository(*repositoriesResponse, *repositoryName)
	if err != nil {
		log.Fatal(err)
	}

	// Part A
	repositoryID := repository.Id.String()
	searchCriteria := git.GitPullRequestSearchCriteria{
		Status: &git.PullRequestStatusValues.All,
	}
	pullRequests, err := gitClient.GetPullRequests(ctx, git.GetPullRequestsArgs{RepositoryId: &repositoryID, SearchCriteria: &searchCriteria, Top: prAmountToQuery})
	if err != nil {
		log.Fatal(err)
	}

	// Part B
	fmt.Printf("🚀 Own commits on the latest %d PRs:\n", *prAmountToQuery)
	commitsContainerList := getPullRequestCommits(ctx, gitClient, pullRequests, &repositoryID)
	commitsDatedList := mapCommitsContainersList(commitsContainerList, func(c commitsContainer) commitsDated {
		return commitsDated{
			PullRequestID:    c.PullRequestID,
			PullRequestTitle: c.PullRequestTitle,
			DatesWithCommits: getDatesWithCommits(c.Commits),
		}
	})

	timeloggableItemMap := commitsDatedListToMap(commitsDatedList)
	commitKeys := make([]string, 0, len(timeloggableItemMap))
	for k := range timeloggableItemMap {
		commitKeys = append(commitKeys, k)
	}
	sort.Strings(commitKeys)
	for _, k := range commitKeys {
		formattedList := mapTimeLoggableItemList(timeloggableItemMap[k], func(item timeloggableItem) string {
			return item.print()
		})
		fmt.Println(k, strings.Join(formattedList, ", "))
	}
	fmt.Println()

	// Part C
	fmt.Printf("💬 Code Reviews of the latest %d PRs:\n", *prAmountToQuery)

	prCommentThreadsContainerList := getPullRequestCommentThreads(ctx, gitClient, pullRequests, &repositoryID)
	prCommentThreadsDatedList := mapPrCommentsContainerList(prCommentThreadsContainerList, func(c codeReviewsContainer) codeReviewsDated {
		return codeReviewsDated{
			PullRequestID:     c.PullRequestID,
			PullRequestTitle:  c.PullRequestTitle,
			DatesWithComments: getDatesWithComments(c.CommentThreads),
		}
	})

	timeloggableItemMapCodeReview := codeReviewsDatedListToMap(prCommentThreadsDatedList)
	codeReviewKeys := make([]string, 0, len(timeloggableItemMapCodeReview))
	for k := range timeloggableItemMapCodeReview {
		codeReviewKeys = append(codeReviewKeys, k)
	}
	sort.Strings(codeReviewKeys)
	for _, k := range codeReviewKeys {
		formattedList := mapTimeLoggableItemList(timeloggableItemMapCodeReview[k], func(item timeloggableItem) string {
			return item.print()
		})
		fmt.Println(k, "Code Review", strings.Join(formattedList, ", "))
	}
}

func setupAndCheckFlags() {
	userName = flag.String("userName", "", "the name of the targeted user on Azure DevOps. Example: John Doe")
	organizationURLSlug = flag.String("organizationURLSlug", "", "The URL slug of your organization, usually an abbreviation of your company. Either https://dev.azure.com/{yourorganization} or https://{yourorganization}.visualstudio.com/ works.")
	personalAccessToken = flag.String("personalAccessToken", "", "The PAT that allows for API access. Only code:read scope needs to be granted. See also https://docs.microsoft.com/en-us/azure/devops/organizations/accounts/use-personal-access-tokens-to-authenticate")
	projectName = flag.String("projectName", "", "The project name that contains the repo that you want to analyze. Example: example.org/{projectName}/_git/{repositoryName}")
	ticketPrefix = flag.String("ticketPrefix", "", "The prefix of your tickets. For Jira-Projects, all ticket IDs are like EXAMPLE-1234. Use 'EXAMPLE-' including the '-' in this case. If your ticket system has no prefix, leave this empty.")
	repositoryName = flag.String("repositoryName", "", "The name of the repository that you want to analyze. Example: example.org/{projectName}/_git/{repositoryName}")
	prAmountToQuery = flag.Int("queryAmount", 25, "The amount of Pull Requests to inspect. Use less than 100.")

	flag.Parse()

	if *userName == "" {
		flag.PrintDefaults()
		log.Fatalln("Please specify an userName")
	} else if *organizationURLSlug == "" {
		flag.PrintDefaults()
		log.Fatalln("Please specify an organizationURLSlug")
	} else if *personalAccessToken == "" {
		flag.PrintDefaults()
		log.Fatalln("Please specify a personalAccessToken")
	}

	if *ticketPrefix == "" {
		fmt.Println("Info: The ticketPrefix is empty. Ths is only correct if your ticket IDs only contain digits.")
	}

}

func findRepository(repositories []git.GitRepository, repositoryName string) (git.GitRepository, error) {
	for _, repository := range repositories {
		if *repository.Name == repositoryName {
			return repository, nil
		}
	}
	return git.GitRepository{}, fmt.Errorf("the specified repository '%s' was not found (possibly, the project name is wrong)", repositoryName)
}

func findProjectID(projects []core.TeamProjectReference, projectName string) (string, error) {
	for _, project := range projects {
		if *project.Name == projectName {
			return project.Id.String(), nil
		}
	}
	return "", fmt.Errorf("the specified project name '%s' was not found", projectName)
}

type commitsContainer struct {
	PullRequestID    int
	PullRequestTitle string
	Commits          *[]git.GitCommitRef
}

func getPullRequestCommits(
	ctx context.Context,
	gitClient git.Client,
	pullRequests *[]git.GitPullRequest,
	repositoryID *string,
) []commitsContainer {
	var containerList []commitsContainer

	for _, pullRequest := range *pullRequests {
		args := git.GetPullRequestCommitsArgs{RepositoryId: repositoryID, PullRequestId: pullRequest.PullRequestId}
		commits, err := gitClient.GetPullRequestCommits(ctx, args)
		if err != nil {
			fmt.Printf("could not get commits for %d \n", pullRequest.PullRequestId)
		}

		containerList = append(containerList, commitsContainer{
			PullRequestID:    *pullRequest.PullRequestId,
			PullRequestTitle: *pullRequest.Title,
			Commits:          &commits.Value,
		})
	}

	return containerList
}

func mapTimeLoggableItemList(itemList []timeloggableItem, f func(item timeloggableItem) string) []string {
	stringList := make([]string, len(itemList))
	for i, v := range itemList {
		stringList[i] = f(v)
	}
	return stringList
}

func (item timeloggableItem) print() string {
	if strings.Contains(item.PullRequestTitle, "No-Ticket") {
		return fmt.Sprintf("PR %d (%s)", item.PullRequestID, item.PullRequestTitle)
	}

	// regexr.com/5ksn4
	regexRule := fmt.Sprintf(`\[?%s\d+\]?`, *ticketPrefix)
	regex := regexp.MustCompile(regexRule)
	match := regex.FindString(item.PullRequestTitle)

	if match != "" {
		return fmt.Sprintf("%s", match)
	}

	return fmt.Sprintf("PR with invalid Name '%s'", item.PullRequestTitle)
}

func getDatesWithCommits(commits *[]git.GitCommitRef) []time.Time {
	var activeDates []time.Time
	for _, commit := range *commits {
		if commit.Author.Name == nil || !strings.EqualFold(*commit.Author.Name, *userName) {
			continue
		}
		activeDates = append(activeDates, commit.Committer.Date.Time)
	}

	return activeDates
}

func mapCommitsContainersList(commitsContainerList []commitsContainer, f func(c commitsContainer) commitsDated) []commitsDated {
	commitsDated := make([]commitsDated, len(commitsContainerList))
	for i, v := range commitsContainerList {
		commitsDated[i] = f(v)
	}
	return commitsDated
}

func commitsDatedListToMap(elements []commitsDated) map[string][]timeloggableItem {
	timeloggableItemMap := make(map[string][]timeloggableItem)
	for _, v := range elements {
		for _, date := range v.DatesWithCommits {
			formattedDate := date.Format("2006-01-02")
			timeloggableItemMap[formattedDate] = appendIfMissing(timeloggableItemMap[formattedDate], timeloggableItem{
				PullRequestID:    v.PullRequestID,
				PullRequestTitle: v.PullRequestTitle,
			})
		}
	}
	return timeloggableItemMap
}

func getPullRequestCommentThreads(
	ctx context.Context,
	gitClient git.Client,
	pullRequests *[]git.GitPullRequest,
	repositoryID *string,
) []codeReviewsContainer {

	var containerList []codeReviewsContainer

	for _, pullRequest := range *pullRequests {

		threadsArgs := git.GetThreadsArgs{RepositoryId: repositoryID, PullRequestId: pullRequest.PullRequestId}
		threads, err := gitClient.GetThreads(ctx, threadsArgs)
		if err != nil {
			fmt.Printf("could not get threads for %d \n", pullRequest.PullRequestId)

		}

		containerList = append(containerList, codeReviewsContainer{
			PullRequestID:    *pullRequest.PullRequestId,
			PullRequestTitle: *pullRequest.Title,
			CommentThreads:   threads,
		})
	}

	return containerList
}

func mapPrCommentsContainerList(codeReviewsContainerList []codeReviewsContainer, f func(c codeReviewsContainer) codeReviewsDated) []codeReviewsDated {
	codeReviewsDated := make([]codeReviewsDated, len(codeReviewsContainerList))
	for i, v := range codeReviewsContainerList {
		codeReviewsDated[i] = f(v)
	}
	return codeReviewsDated
}

func getDatesWithComments(commentThreadList *[]git.GitPullRequestCommentThread) []time.Time {
	var activeDates []time.Time
	for _, commentThread := range *commentThreadList {
		for _, comment := range *commentThread.Comments {
			if !strings.EqualFold(*comment.Author.DisplayName, *userName) {
				continue
			}
			activeDates = append(activeDates, comment.PublishedDate.Time)
		}
	}

	return activeDates
}

func codeReviewsDatedListToMap(elements []codeReviewsDated) map[string][]timeloggableItem {
	timeloggableItemMap := make(map[string][]timeloggableItem)
	for _, v := range elements {
		for _, date := range v.DatesWithComments {
			formattedDate := date.Format("2006-01-02")
			timeloggableItemMap[formattedDate] = appendIfMissing(timeloggableItemMap[formattedDate], timeloggableItem{
				PullRequestID:    v.PullRequestID,
				PullRequestTitle: v.PullRequestTitle,
			})
		}
	}
	return timeloggableItemMap
}

func mapTimeSliceToStringSlice(vs []time.Time, f func(time.Time) string) []string {
	vsm := make([]string, len(vs))
	for i, v := range vs {
		vsm[i] = f(v)
	}
	return vsm
}

func mapProjectSliceToStringSlice(projects []core.TeamProjectReference, f func(project core.TeamProjectReference) string) []string {
	strings := make([]string, len(projects))
	for i, v := range projects {
		strings[i] = f(v)
	}
	return strings
}

func mapRepositorySliceToStringSlice(repositories []git.GitRepository, f func(repository git.GitRepository) string) []string {
	repositoryStringSlice := make([]string, len(repositories))
	for i, v := range repositories {
		repositoryStringSlice[i] = f(v)
	}
	return repositoryStringSlice
}

// appendIfMissing adapted from https://stackoverflow.com/a/9561388/3991578
func appendIfMissing(slice []timeloggableItem, i timeloggableItem) []timeloggableItem {
	for _, ele := range slice {
		if ele == i {
			return slice
		}
	}
	return append(slice, i)
}

type commitsDated struct {
	PullRequestID    int
	PullRequestTitle string
	DatesWithCommits []time.Time
}

type timeloggableItem struct {
	PullRequestID    int
	PullRequestTitle string
}

type timeloggableGroup struct {
	items []timeloggableItem
}

type codeReviewsContainer struct {
	PullRequestID    int
	PullRequestTitle string
	CommentThreads   *[]git.GitPullRequestCommentThread
}

type codeReviewsDated struct {
	PullRequestID     int
	PullRequestTitle  string
	DatesWithComments []time.Time
}
