# devops-timelog

This tool is useful to automate timelogging of dev tasks (own development & code reviews).

The only hard pre-requisite: You must be using Azure DevOps.

## Installation

### Option A: Use precompiled binaries

Download the latest binary for your machine at [the releases section](https://gitlab.com/carstenhag/devops-timelog/-/releases) of the project. Then, run it via your terminal.

### Option B: Setup Go and compile it on your own (it's fast, I promise!)

Refer to the golang website to [install Go](https://golang.org/dl/) and to the Go Wiki to [configure the environment](https://golang.org/doc/gopath_code.html). Most importantly, `go` must be available, and your shell `PATH` must contain `$GOPATH/bin`.

Install this tool with `go get gitlab.com/carstenhag/devops-timelog`.

## Usage

After having downloaded or installed the binary, you can run it now!  
In case you selected Option A, when running the following commands you must specify the path to it. Example: `./devops-timelog`.

This is the command to start the tool. Make sure to replace all values with your own, see [the CLI Parameters](#cli-parameters) section for that.

```devops-timelog --userName "My Name" --organizationURLSlug=test --projectName=test --repositoryName=test --personalAccessToken=TOKEN_HERE --ticketPrefix=EXAMPLE-```


To make things easier, you can save the above command as an alias to your shell config file (.zshrc, .bashrc, ...). Example:

```alias timelog='devops-timelog --userName "Test" --organizationURLSlug=test --projectName=test --repositoryName=test --personalAccessToken=TOKEN_HERE  --ticketPrefix=EXAMPLE-'```


### CLI Parameters

```
  --organizationURLSlug string
        The URL slug of your organization, usually an abbreviation of your company. Either https://dev.azure.com/{yourorganization} or https://{yourorganization}.visualstudio.com/ works.
  --personalAccessToken string
        The PAT that allows for API access. Only code:read scope needs to be granted. See also https://docs.microsoft.com/en-us/azure/devops/organizations/accounts/use-personal-access-tokens-to-authenticate
  --projectName string
        The project name that contains the repo that you want to analyze. Example: example.org/{projectName}/_git/{repositoryName}
  --queryAmount int
        The amount of Pull Requests to inspect. Use less than 100. (default 25)
  --repositoryName string
        The name of the repository that you want to analyze. Example: example.org/{projectName}/_git/{repositoryName}
  --ticketPrefix string
        The prefix of your tickets. For Jira-Projects, all ticket IDs are like EXAMPLE-1234. Use 'EXAMPLE-' including the '-' in this case. If your ticket system has no prefix, leave this empty.
  --userName string
        the name of the targeted user on Azure DevOps. Example: John Doe
```

## Script description

Part A - Intake  
1: Get last X Pull Requests, the default is 25.


Part B - Dev commits  
1: For each Pull Request, get all of its commits.  
2: For each day on which the user committed on a specific Pull Requests: Store the day, PR ID and PR Title.  
3: Print out all commits for a single day.


Part C - Code Reviews  
1: For each Pull Request, get all of its comment threads.  
2: For each day on which the user commented on a specific Pull Request: Store the day, PR ID and PR Title.  
3: Print out all code reviews for a single day.


